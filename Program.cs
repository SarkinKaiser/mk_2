﻿using System;
using System.IO;
using System.Text;


namespace Kr_2
{
    class Pppppprogram
    {
        public static void Main(string[] args)
        {
            Console.OutputEncoding = UTF8Encoding.UTF8;

            Console.WriteLine("Введіть шлях до файлу: ");
            string Шлях = Console.ReadLine();
            Console.WriteLine("Введіть рядок символів який потрібно знайти:");
            string Шаблон = Console.ReadLine();

            string Текст = "";
            try
            {
                Текст = File.ReadAllText(Шлях);
            }
            catch (FileNotFoundException Виключення)
            {
                Console.WriteLine("Файл за шляхом " + Шлях + " не знайдено ");
                Console.ReadLine();
                return;
            }

            try
            {
                int position = FindSubstring(Текст, Шаблон);
                Console.WriteLine(position);
            }
            catch (ArgumentException Виключення)
            {
                Console.WriteLine(Виключення);
            }
            Console.ReadLine();
        }



        private static int FindSubstring(string Введення, string Шаблон)
        {
            int v = Введення.Length;
            int h = Шаблон.Length;
            if (0 == v)
                throw new ArgumentException("", "Введення");
            if (0 == h)
                throw new ArgumentException("", "Шаблон");

            int[] d = GetPrefixFunction(Шаблон);

            int z, r;
            for (z = 0, r = 0; (z < v) && (r < h); z++, r++)
                while ((r >= 0) && (Шаблон[r] != Введення[z]))
                    r = d[r];

            if (r == h)
                return z - r;
            else
                return -1;
        }



        private static int[] GetPrefixFunction(string Шаблон)
        {
            int Довжина = Шаблон.Length;
            int[] Рузультат = new int[Довжина];

            int n = 0, x = -1;
            Рузультат[0] = -1;
            while (n < Довжина - 1)
            {
                while ((x >= 0) && (Шаблон[x] != Шаблон[n]))
                    x = Рузультат[x];
                n++;
                x++;
                if (Шаблон[n] == Шаблон[x])
                    Рузультат[n] = Рузультат[x];
                else
                    Рузультат[n] = x;
            }
            return Рузультат;
        }
    }
}
